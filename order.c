#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_FOODS 100
#define MAX_COMBOS 100
#define MAX_ORDERS 54001
#define TIME_LENGTH 9
#define NAME_LENGTH 50

typedef struct {
    char name[NAME_LENGTH];
    int time_to_make;
    int capacity;
    int current_stock;
    int making_time;
    int down_time;
} Food;

typedef struct {
    char name[NAME_LENGTH];
    int num_items;
    int items[MAX_FOODS];
} Combo;

typedef struct {
    int hour;
    int minute;
    int second;
} Time;

typedef struct {
    Time order_time;
    char name[NAME_LENGTH];
    int type; 
    int id;
    int items_remaining;
    int items_needed[MAX_FOODS];
    Time completion_time;
    int fulfilled; 
    int failed; 
} Order;

Food foods[MAX_FOODS];
Combo combos[MAX_COMBOS];
Order orders[MAX_ORDERS];
int num_foods = 0;
int num_combos = 0;
int num_orders = 0;

int W1, W2;

int cur_handle = 0;
int cur_wait = 0;

int system_closed = 0;
Time closing_time = {22, 0, 0};
Time max_time = {23, 59, 59};

Food *get_food_by_type(char* type){
    for(int i=0;i<num_foods;i++) {
        if(strcmp(foods[i].name, type) == 0) {
            return  &foods[i];
        }
    }
    return NULL;
}

Time parse_time(const char* str) {
    Time t;
    sscanf(str, "%d:%d:%d", &t.hour, &t.minute, &t.second);
    return t;
}

int time_to_seconds(Time t) {
    return t.hour * 3600 + t.minute * 60 + t.second;
}

Time seconds_to_time(int seconds) {
    Time t;
    t.hour = seconds / 3600;
    t.minute = (seconds % 3600) / 60;
    t.second = seconds % 60;
    return t;
}

void read_menu(const char* filename) {
    FILE* file = fopen(filename, "r");
    int i;
    fscanf(file, "%d %d", &num_foods, &num_combos);
    for (i = 0; i < num_foods; i++) {
        fscanf(file, "%s", foods[i].name);
    }
    for (i = 0; i < num_foods; i++) {
        fscanf(file, "%d", &foods[i].time_to_make);
    }
    for (i = 0; i < num_foods; i++) {
        fscanf(file, "%d", &foods[i].capacity);
    }
    fscanf(file, "%d %d", &W1, &W2);
    for (i = 0; i < num_combos; i++) {
        fscanf(file, "%s", combos[i].name);
        int j = 0;
        char item_name[NAME_LENGTH];
        while (fscanf(file, "%s", item_name) != EOF && fgetc(file) != '\n') {
            int k;
            for (k = 0; k < num_foods; k++) {
                if (strcmp(item_name, foods[k].name) == 0) {
                    combos[i].items[j] = k;
                    break;
                }
            }
            j++;
        }
        combos[i].num_items = j;
    }

    fclose(file);
}

void read_orders() {
    int i, j;
    scanf("%d", &num_orders);

    // 初始化菜单，让每一个的库存都为0
    for (i = 0; i < num_foods; i++) {
        foods[i].current_stock = 0;
        foods[i].making_time = foods[i].time_to_make;
    }
    // 读取订单信息到订单列表
    for (i = 0; i < num_orders; i++) {
        int k;
        char time_str[TIME_LENGTH];
        char type_name[NAME_LENGTH];
        scanf("%s %s", time_str, type_name);
        orders[i].order_time = parse_time(time_str);
        int found = 0;
        for (j = 0; j < num_combos; j++) {
            if (strcmp(type_name, combos[j].name) == 0) {
                orders[i].type = 1;
                orders[i].id = j;
                for (k = 0; k < combos[j].num_items; k++) {
                    int food_index = combos[j].items[k];
                    orders[i].items_needed[food_index]++;
                    orders[i].items_remaining++;
                }
                found = 1;
                break;
            }
        }
        if (!found) {
            for (j = 0; j < num_foods; j++) {
                if (strcmp(type_name, foods[j].name) == 0) {
                    // 表示不是套餐
                    orders[i].type = 0;
                    orders[i].id = j;
                    orders[i].items_needed[j]++;
                    orders[i].items_remaining++;
                    found = 1;
                    break;
                }
            }
        }
        strcpy(orders[i].name, type_name);
        orders[i].fulfilled = 0;
        orders[i].failed = 0;
    }
}

void update_food_stock() {
    int i;
    for (i = 0; i < num_foods; i++) {
        foods[i].making_time--;
        foods[i].down_time ++;
        if (foods[i].down_time == foods[i].time_to_make) {
            foods[i].current_stock++;
            foods[i].down_time = 0;
        }
        if (foods[i].current_stock < foods[i].capacity && foods[i].making_time == 0) {
            foods[i].making_time = foods[i].time_to_make;
        }
    }
}

int get_min_wait_time(char* type){
    // 有订单在做
    for(int i=0;i<cur_handle;i++) {
        Order o = orders[i];
        if(strcmp(o.name, type) == 0 && o.items_remaining >0) {
            return o.items_remaining + get_food_by_type(type)->making_time ;
        }
    }
    // 有自动触发的备货在做
    Food *f = get_food_by_type(type);
    if(f->making_time > 0 && f->current_stock == 0) {
        return f->making_time + f->time_to_make ;
    }
    // 都没有
    return f->time_to_make;
}

Combo *get_combo(char* type){
    for(int i=0;i<num_combos;i++) {
        Combo *c = &combos[i];
        if(strcmp(type, c->name) == 0) {
            return c;
        }
    }
    return NULL;
}

void process_orders() {
    int current_time = time_to_seconds((Time){7, 0, 0});
    int end_time = time_to_seconds(closing_time);
    int max_seconds = time_to_seconds(max_time);
    int pending_open = 1;

    while (current_time <= end_time || cur_wait > 0) {

        // 时间正常流逝
        current_time++;
        update_food_stock();

        Order* cur_order = &orders[cur_handle];

        // 遍历订单，获取完成了的
        for(int i=0;i<cur_handle;i++) {
            Order *o = &orders[i];
            o->items_remaining --;
            if(o->items_remaining == 0) {
                cur_wait --;
                o->fulfilled = 1;
                o->completion_time = seconds_to_time(current_time);
            }
        }

        // 判断当前订单数量是否可以接受订单
        if (cur_wait < W2 && pending_open) {
            system_closed = 0;
            // 判断当前处理的订单是否有正在处理的
        }

        // 没有关闭系统，处理订单
        if(!system_closed && time_to_seconds(cur_order->order_time) <= current_time && strlen(cur_order->name)) {
            // 判断订单是否到达

            // 判断订单是不是套餐
            // 不是套餐
            if (orders[cur_handle].type == 0) {
                Food *f = get_food_by_type(cur_order->name);
                // 判断当前是否有库存
                if (f->current_stock > 0) {
                    // 减少库存
                    f->current_stock--;
                    if(f->making_time <= 0 && f->current_stock < f->capacity) {
                        f->making_time = f->time_to_make;
                    }

                    // 该订单被处理
                    cur_order->fulfilled = 1;
                    cur_order->failed = 0;
                    cur_order->items_remaining = -1;
                    cur_order->completion_time = seconds_to_time(current_time);
                }
                // 没有库存，则添加到等待列表
                else {
                    int tag = get_min_wait_time(cur_order->name);
                    // 有订单正在做的任务
                    if(tag > 0) {
                        cur_order->items_remaining = tag;
                    } else {
                        cur_order->items_remaining = f->time_to_make;
                    }
                    cur_wait++;
                    cur_order->failed = 0;

                }
                // 继续处理下一个
                cur_handle++;
            }

            // 套餐
            else if(cur_order->type == 1) {
                // 判断是否所有都有库存
                int tag = 1;
                Combo *combo = get_combo(cur_order->name);
                for(int i=0;i<combo->num_items;i++) {
                    int index = combo->items[i];
                    if(foods[index].current_stock == 0) {
                        tag = 0;
                        break;
                    }
                }
                if(tag) {
                    for(int i=0;i<combo->num_items;i++) {
                        int index = combo->items[i];
                        foods[index].current_stock --;
                        foods[index].making_time = foods[index].time_to_make;
                    }

                    // 该订单被处理
                    cur_order->fulfilled = 1;
                    cur_order->failed = 0;
                    cur_order->items_remaining = -1;
                    cur_order->completion_time = seconds_to_time(current_time);


                }
                // 若不是都有库存则记录最长等待时间的商品作为订单完成时间
                else {
                    int max = 0;
                    // 先找出缺失的最长等待时间时间
                    for(int i=0;i<combo->num_items;i++) {
                        int index = combo->items[i];
                        if(foods[index].current_stock == 0 && max < get_min_wait_time(foods[index].name) ) {
                            max = get_min_wait_time(foods[index].name) ;
                        }
                    }
                    for(int i=0;i<combo->num_items;i++) {
                        int index = combo->items[i];
                        // 减少已有库存
                        if(foods[index].current_stock) {
                            foods[index].current_stock --;
                        }
                        foods[index].making_time = get_min_wait_time(foods[index].name);
                    }
                    // 添加订单任务
                    cur_order->items_remaining = max  ;

                    cur_wait++;
                    cur_order->failed = 0;
                }
                cur_handle++;
//                    // 若不是都有库存则记录最长等待时间的商品作为订单完成时间
//                    for(int j=0;j<num_combos;j++){
//                        if()
//                    }

            }

            // 判断当前订单数是否超过最大等待，超过则跳出循环，实现关店效果
            if (cur_wait > W1) {
                system_closed = 1;
            }

        }
        // 如果点餐时系统关闭，会导致点餐失败
        else if(system_closed&&time_to_seconds(cur_order->order_time) <= current_time && strlen(cur_order->name) ){
            cur_order->failed = -1;
            cur_order->items_remaining = -1;
            cur_handle ++;
        }

        if (current_time > max_seconds){
            break;
        }


    }
}



void output_results() {
    int i;
    for (i = 0; i < num_orders; i++) {
        if (orders[i].fulfilled == 1) {
            Time completion_time = orders[i].completion_time;
            printf("%02d:%02d:%02d\n",
                   completion_time.hour,
                   completion_time.minute,
                   completion_time.second);
        } else if (orders[i].failed == -1) {
            printf("Fail\n");
        } else {
            printf("Unknown\n");
        }
    }
}

int main() {
    read_menu("dict.dic");
    read_orders();
    process_orders();
    output_results();
    return 0;
}

